pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
import "../GameObject/GameObject.sol";

contract BaseStation is GameObject {
    
    uint private _strengthOfDefense;
    bool private _isAlive;

    mapping (address=> GameObject) public units; 

    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();

        _isAlive = true;
        hitPoints = 100;
        _strengthOfDefense = 30; 
    }
    
    function addMilitaryUnit(GameObject obj) public checkOwnerAndAccept{
        if(_isAlive == true)
            units[obj] = obj;
    }

    function deleteMilitaryUnit(GameObject obj) public checkOwnerAndAccept{  
        if(_isAlive == true)    
             delete units[obj];
    }


    function destroy(address dest) override public {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();

        optional(address, GameObject) cOpt = units.min();
        while(cOpt.hasValue()){ (address key, GameObject data) = cOpt.get();

            data.destroy(dest); 
            cOpt =  units.next(key);
        }
        dest.transfer(1, true, 160);
    }

}
