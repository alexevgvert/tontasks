pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
import "GameObjectInterface.sol";

abstract contract GameObject is GameObjectInterface {
     
    uint private _strengthOfDefense;
    uint public hitPoints; 
    bool private _isAlive;

    modifier checkOwnerAndAccept {
        require(msg.pubkey() == tvm.pubkey(), 100);
        tvm.accept();
        _;
    }
 
    function takeTheAttack(uint attackPower, address enemy) override external checkOwnerAndAccept{

        uint damage =  attackPower - _strengthOfDefense;

        if(_isAlive == true)
        {
           if((hitPoints - damage) > 0) {
               hitPoints = hitPoints - damage;
           }
           else {
               _isAlive = false;
               destroy(enemy);
           }
        }
    }

    function getStrengthOfDefense() view external returns (uint) {
        return _strengthOfDefense;
    }

    function destroy(address dest) virtual public {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        dest.transfer(1, true, 160);
    }
}
