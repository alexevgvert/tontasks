pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
import "../Units/MilitaryUnit.sol";

contract Warrior is MilitaryUnit {
    uint private _attackPower = 20;
    uint private _strengthOfDefense = 20;

    constructor(BaseStation bStation) MilitaryUnit(bStation) public {}
}
