pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
import "../BaseStation/BaseStation.sol";

contract MilitaryUnit is GameObject{
    uint private _attackPower;
    uint private _strengthOfDefense;
    bool private _isAlive = true;
    address addrStation;
    address myAddr;
    
    constructor(BaseStation bStation) public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        myAddr = msg.sender;
        hitPoints = 100; 
        bStation.addMilitaryUnit(this);
        addrStation = bStation;
    }

    
    function destroy(address dest) override public {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        if(msg.sender == myAddr || msg.sender == addrStation){
            BaseStation station = BaseStation(addrStation);
            station.deleteMilitaryUnit(this);
            dest.transfer(1, true, 160);
        }
    }
    
    function attack(GameObject enemy) virtual external checkOwnerAndAccept{
        enemy.takeTheAttack(_attackPower, msg.sender);
    }

    function getAttackPower() view external returns (uint) {
        return _attackPower;
    }
    
}
