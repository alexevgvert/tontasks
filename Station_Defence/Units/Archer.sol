pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
import "../Units/MilitaryUnit.sol";

contract Archer is MilitaryUnit {
    uint private _attackPower = 40;
    uint private _strengthOfDefense = 10;

    constructor(BaseStation bStation) MilitaryUnit(bStation) public {}
}
