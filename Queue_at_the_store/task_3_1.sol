pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

// "Очередь в магазине"
contract task_3_1 {

    string[] public queue;
   
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier checkOwnerAndAccept() {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        _;
    }
 
    // Встать в очередь
    function Enqueue(string name) public checkOwnerAndAccept {
         queue.push(name);
    }


    // Выйти из очереди
    function Dequeue() public checkOwnerAndAccept {
         
        if(!queue.empty()){
           for (uint256 i = 0; i < queue.length-1; i++) {
             queue[i] = queue[i+1];
           }
           queue.pop();
        }
    }
}
