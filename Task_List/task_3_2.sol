pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

// "Список задач"
contract task_3_2 {
   
    struct task {
        string name;
        uint32 timestamp;
        bool isCompleted;
    }

    int8 task_index = 0;
    mapping(int8 => task) tasks;
    
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier checkOwnerAndAccept() {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        _;
    }

    // Добавление задачи
    function addTask(string name, bool isCompleted) public checkOwnerAndAccept {
        tasks[task_index] = task(name,now,isCompleted);
        task_index++;
    }

    // Получить количество открытых задач
    function getNumberOpenTask() public checkOwnerAndAccept returns (int8){
        optional(int8, task) cOpt = tasks.min();
        int8 numberOpenTask = 0;

        while(cOpt.hasValue()){
            (int8 key,task data) = cOpt.get();

            if(data.isCompleted) {
                numberOpenTask++;
            }
            cOpt =  tasks.next(key);
        }
         
        return numberOpenTask;
    }

    // Получить получить список задач
    function getTasks() public checkOwnerAndAccept returns (mapping(int8 => task)){
        return tasks;
    }

    // Получить описание задачи по ключу
    function getTask(int8 key) public checkOwnerAndAccept returns (task){
        return tasks[key];
    }

    // Удалить задачу по ключу
    function deleteTask(int8 key) public checkOwnerAndAccept{
        if(tasks.exists(key)){
            delete tasks[key];
        }
    }

    // Отметить задачу как выполненную по ключу
    function setIsCompletedTrueInTask(int8 key) public checkOwnerAndAccept{
        if(tasks.exists(key))
        {
            tasks[key].isCompleted = true;
        }
    }
}
