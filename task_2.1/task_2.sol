pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract task_2 {
     
    uint public multiplier = 1;

    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier checkOwnerAndAccept() {
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
        _;
    }
 
    function multiplyByNumber(uint value) public checkOwnerAndAccept {
        require(value >= 1 && value <= 10, 101, "Wrong number!!");
        multiplier *= value;
    }
}
