pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract HeroToken {
 
    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    // Проверка на уникальность имени
    modifier checkHeroNameForUniqueness(string name) {
        for (uint i = 0; i < tokensArr.length; i++) {
            require(tokensArr[i].name != name, 101, "Name is already taken");
        }
        _;
    }

    modifier checkOwnerAndAccept {
        require(msg.pubkey() == tvm.pubkey(), 100);
        tvm.accept();
        _;
    }
     
    enum State { ForSale, NotForSale} // Состояние продажи
    mapping (uint=>uint) tokenToOwner;

    struct Token{
        string name;

        uint mhp; // Maximum HP
        uint mmp; // Maximum MP
        uint atc; // Attack
        uint def; // Defense

        State state; 
        uint128 price;
    }

    Token[] tokensArr;

    function createToken(string name, uint mhp, uint mmp, uint atc, uint def) public checkHeroNameForUniqueness(name) checkOwnerAndAccept{
        tokensArr.push(Token(name, mhp, mmp, atc, def, State.NotForSale, 0));
        tokenToOwner[tokensArr.length - 1] = msg.pubkey();
    }
    
    // Выдать информацию о владельце токена
    function getTokenOwner(uint tokenId) public view returns (uint){
        return tokenToOwner[tokenId];
    }
    
    // Выдать информацию о токене
    function getTokenInfo(uint tokenId) public view returns (string name, uint mhp, uint mmp,  uint atc, uint def, State state, uint128 price){ 
        name = tokensArr[tokenId].name;
        mhp = tokensArr[tokenId].mhp;
        mmp = tokensArr[tokenId].mmp;
        atc = tokensArr[tokenId].atc;
        def = tokensArr[tokenId].def;
        state = tokensArr[tokenId].state;
        price = tokensArr[tokenId].price;
    }

    // Выставить токен на продажу 
    function putTokenForSale(uint tokenId, uint128 price) public checkOwnerAndAccept{
        require(tvm.pubkey() == tokenToOwner[tokenId], 101);
        tokensArr[tokenId].state = State.ForSale;
        tokensArr[tokenId].price = price;
    }
}
