pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;


contract Wallet {

    constructor() public {
        require(tvm.pubkey() != 0, 101);
        require(msg.pubkey() == tvm.pubkey(), 102);
        tvm.accept();
    }

    modifier checkOwnerAndAccept {
        require(msg.pubkey() == tvm.pubkey(), 100);
		tvm.accept();
		_;
	}

    function sendTransaction(address dest, uint128 value, bool bounce, uint16 flag) public pure checkOwnerAndAccept {
        dest.transfer(value, bounce, flag);
    }

    // Отправить без оплаты комиссии за свой счет
    function sendTransactionWithoutPayingCommission(address dest, uint128 value, bool bounce) public pure checkOwnerAndAccept {
        dest.transfer(value, bounce, 0);
    }

    // Отправить с оплатой комисси за свой счет
    function sendTransactionWithPaymentOfCommission (address dest, uint128 value, bool bounce) public pure checkOwnerAndAccept {
        dest.transfer(value, bounce, 1);
    }

    // Отправить все деньги и уничтожить кошелек
    function sendAllMoneyAndDestroyTheWallet (address dest, uint128 value, bool bounce) public pure checkOwnerAndAccept {
        dest.transfer(value, bounce, 160);
    }
}