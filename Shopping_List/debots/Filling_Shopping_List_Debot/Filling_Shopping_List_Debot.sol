pragma ton-solidity >=0.35.0;
pragma AbiHeader expire;
pragma AbiHeader time;
pragma AbiHeader pubkey;
import '../abstract/Base_Shopping_List_Debot.sol';

contract Filling_Shopping_List_Debot is Base_Shopping_List_Debot {

    uint64 _newPurchaseSize;
    string _newPurchaseName;

    function _menu() internal override {
        string sep = '----------------------------------------';
        Menu.select(
            format(
                "You have {}/{}/{} (paid/unpaid/total) purchases with total price = {} : ",
                    m_stat.numberPaid,
                    m_stat.numberUnpaid,
                    m_stat.numberPaid + m_stat.numberUnpaid,
                    m_stat.totalCost
            ),
            sep,
            [
                MenuItem("Add product to shopping basket","",tvm.functionId(addNewPurchaseToList)),
                MenuItem("Show purchase list","",tvm.functionId(showPurchaseList)),
                MenuItem("Delete purchase","",tvm.functionId(deletePurchase))
            ]
        );
    }

    function addNewPurchaseToList(uint32 index) public {
        Terminal.input(tvm.functionId(selectNewName),"Enter purchase name:", false);
        Terminal.input(tvm.functionId(selectNewSize),"Enter purchase size:", false);
        Terminal.print(tvm.functionId(addPurchase_), "The purchase is being added to the list now!!");
    }

    function selectNewName(string value) public {
        _newPurchaseName  = value;
    }

    function selectNewSize(string value) public {
        (uint256 num,) = stoi(value);
        _newPurchaseSize = uint64(num);
    }

    function addPurchase_() public {
        optional(uint256) pubkey = 0;
        IShopping_List(m_address).addPurchaseToList{
                abiVer: 2,
                extMsg: true,
                sign: true,
                pubkey: pubkey,
                time: uint64(now),
                expire: 0,
                callbackId: tvm.functionId(onSuccess),
                onErrorId: tvm.functionId(onError)
            }(_newPurchaseName,_newPurchaseSize);
    }

    function getDebotInfo() public functionID(0xDEB) override view returns(
        string name, string version, string publisher, string key, string author,
        address support, string hello, string language, string dabi, bytes icon
    ) {
        name = "Filling Shopping List Debot";
        version = "0.1.0";
        publisher = "TON Labs";
        key = "Filling Shopping List Debot";
        author = "Flegentov Kirill";
        support = address.makeAddrStd(0, 0x66e01d6df5a8d7677d9ab2daf7f258f1e2a7fe73da5320300395f99e01dc3b5f);
        hello = "Hi, i'm a Filling Shopping List DeBot.";
        language = "en";
        dabi = m_debotAbi.get();
        icon = m_icon;
    }
}