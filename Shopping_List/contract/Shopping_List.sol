pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;
pragma AbiHeader pubkey;
import "./IShopping_List.sol";

contract Shopping_List {
    modifier onlyOwner() {
        require(msg.pubkey() == _ownerPubkey, 101);
        _;
    }

    uint256 _ownerPubkey;
    uint32  _count = 1;

    mapping (uint32 => Purchase) _purchases;

    constructor( uint256 pubkey) public {
        require(pubkey != 0, 120);
        tvm.accept();
        _ownerPubkey = pubkey;
    }

    function addPurchaseToList(string name, uint64 size) public onlyOwner
    {
        tvm.accept();
        _purchases[_count] = Purchase( _count, name, size, now, false, 0);
        _count++; 
    }

    function deletePurchaseFromList(uint32 id) public onlyOwner
    {
        require(_purchases.exists(id), 102);
        tvm.accept();
        delete _purchases[id];
    }

    function buyPurchase(uint32 id, uint price) public onlyOwner
    {
        require(_purchases.exists(id), 102);
        tvm.accept();
        _purchases[id].isBought = true;
        _purchases[id].price = price;
    }

    function getPurchases() public view returns (Purchase[] purchases)  
    {
        for((uint32 id, Purchase purchase) : _purchases) {
            purchases.push(Purchase(
                purchase.id, 
                purchase.name, 
                purchase.size, 
                purchase.createdAt,
                purchase.isBought,
                purchase.price));
        }
    }

    function getStat() public view returns (Purchase_Stat stat)
    {
        stat = Purchase_Stat(0,0,0);
        for((uint32 id, Purchase purchase) : _purchases) {
            if(purchase.isBought)
            {
                stat.numberPaid++;
                stat.totalCost += purchase.price;
            }
            else
            {
                stat.numberUnpaid++;
            }
        }
    }


}
