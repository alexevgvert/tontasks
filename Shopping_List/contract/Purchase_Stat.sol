pragma ton-solidity >=0.35.0;
pragma AbiHeader expire;
pragma AbiHeader pubkey;

struct Purchase_Stat {
    uint32 numberPaid;    // сколько предметов в списке "оплачено"
    uint32 numberUnpaid;  // сколько предметов в списке "не оплачено"
    uint totalCost;       // на какую сумму всего было оплачено
}
