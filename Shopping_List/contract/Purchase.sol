pragma ton-solidity >=0.35.0;
pragma AbiHeader expire;
pragma AbiHeader pubkey;

struct Purchase {
    uint32 id;        // идентификатор/номер
    string name;      // название
    uint64 size;      // количество (сколько надо купить)
    uint64 createdAt; // когда заведена
    bool isBought;    // флаг, что куплена
    uint price;       // цена, за которую купили
}