pragma ton-solidity >=0.35.0;
pragma AbiHeader expire;
pragma AbiHeader pubkey;
import "./Purchase_Stat.sol"; 
import "./Purchase.sol"; 

interface IShopping_List {
    function getStat() external returns (Purchase_Stat);
    function getPurchases() external returns (Purchase[]);
    function deletePurchaseFromList(uint32 id) external;
    function buyPurchase(uint32 id, uint price) external;
    function addPurchaseToList(string name, uint64 size) external;
}
 